from django.contrib.sessions.models import Session
from django.shortcuts import redirect
from django.contrib.auth import logout

def logout_user(request):
    logout(request)
    return redirect("landing")