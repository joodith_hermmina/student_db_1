import csv
import datetime

import requests
from django.contrib import messages
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
# Create your views here.
from rest_framework.decorators import authentication_classes, permission_classes, api_view
from django.contrib.auth.decorators import login_required
from student_data.permissions import user_passes_test
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import views, status
import requests
from requests.auth import HTTPBasicAuth
from rest_framework.response import Response

from .models import UserGroup


class GetStudentData(views.APIView):
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, rfid, machine_id):
        try:
            print("rfid", rfid)
            print("machine", machine_id)
            user = User.objects.get(rfidvalue=rfid)
            # data = {'roll_number': str(user.new_rollno),
            #         'student_name': str(user.first_name),
            #         'department': str(user.department),
            #         'email': str(user.email),
            #         }
            auth = HTTPBasicAuth('iQube', 'iQube@2022')
            url = 'http://10.1.75.125:25060/api/post-attendance/'
            json = {"student_name": str(user.first_name),
                    "roll_number": str(user.new_rollno),
                    "department": str(user.department),
                    "machine_id": str(machine_id)}
            resp = requests.post(url, auth=auth, json=json, timeout=(3.05, 9.05))
            return JsonResponse(resp.json())
        except Exception as e:
            return Response({'error': 'User does not exist'})


from django.contrib.auth import login, authenticate

from .forms import *
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView


def LoginView(request):
    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        email = request.POST.get('email_input')

        try:
            user = User.objects.filter(email=email)
            user = user[0]

        except:
            messages.error(request, 'Wrong email address')
            return render(request, 'student_data/login_user.html', {'form': form})

        print(user)
        if user:
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user)

        storage = messages.get_messages(request)
        for msg in storage:
            del msg
        storage.used = True
        return redirect('dashboard')



    else:
        form = LoginForm()
        if request.user.is_authenticated:
            if messages.error:
                return render(request, 'student_data/login_user.html', {'form': form})
            return redirect('dashboard')

        return render(request, "student_data/login_user.html", {'form': form})


def check_dashboard_access(user):
    # permission=UserGroup.objects.get(name='AdminUsers')

    if user.perms.filter(name="AdminUsers").exists():
        return True

    return False


def check_admin(user):
    return user.is_superuser


@user_passes_test(check_dashboard_access, login_url='landing')
def get_data(request):
    if request.method == "POST":
        rfid = request.POST.get('rfid', None)
        if rfid:
            roll_no = request.POST.get('rollno', None)
            if roll_no:
                try:
                    user = User.objects.get(new_rollno=roll_no)
                    store = store_rfid(rfid, user.new_rollno)
                    messages.success(request, 'Student data recorded!' + user.new_rollno)
                    return redirect('get_data')
                except User.DoesNotExist:
                    messages.warning(request, 'Returned no student')
        user = None
        roll_no = request.POST.get('rollno', None)
        if roll_no:
            try:
                user = User.objects.get(new_rollno=roll_no)
                show = False
                if len(user.rfidvalue) > 4:
                    show = True
                return render(request, 'student_data/submit_data.html', {'user': user, 'show': show})
            except User.DoesNotExist:
                messages.warning(request, 'Returned no student')
        return render(request, 'student_data/get_data.html', {'user': user})

    return render(request, 'student_data/get_data.html')


def store():
    f = open('error1.txt', 'w')
    json_data = requests.get(
        'http://10.1.68.54/ClientAPI/GetAllStudents?APIKey=321D44719B36C171B7189D6A6DB83&input=12').json()
    for j in json_data:
        if j['Phone_no'] == "":
            phone = None
        elif '-' in str(j['Phone_no']):
            phone = None
        else:
            phone = j['Phone_no']
        if j['Parent_phone_no'] == "":
            parent_phone = None
        elif '-' in str(j['Parent_phone_no']):
            parent_phone = None
        else:
            parent_phone = j["Parent_phone_no"]
        try:
            user = User.objects.create(username=j['RollNo'].upper(), first_name=j['Name'], email=j['Email'],
                                       old_rollno=j['RollNo'].upper(), new_rollno=j['RollNo'].upper(), mobile_no=phone,
                                       batch=j['Batch_year'], department=j['Department'],
                                       entry_type=j['Lateral_Regular'],
                                       parent_phone=parent_phone,
                                       stay=j['Stay'])
            print('User Saved ' + user.new_rollno)
        except Exception as e:
            try:
                user = User.objects.get(username=j['RollNo'].upper())
                user.first_name = j['Name']
                user.email = j['Email']
                user.mobile_no = phone
                user.rollno = j['RollNo'].upper()
                user.batch = j['Batch_year']
                user.department = j['Department']
                user.entry_type = j['Lateral_Regular']
                user.parent_phone = parent_phone
                user.stay = j['Stay']
                user.save()
                print('User Updated ' + user.new_rollno)
            except Exception as e:
                f.write(j['RollNo'] + "\n")

    f.close()


def store_rfid(rfid, user):
    user = User.objects.get(new_rollno=user)
    user.rfidvalue = rfid
    user.save()
    return True


class GetData(APIView):
    def get(self, request):
        roll_no = request.GET.get('roll_no', None)
        email = request.GET.get('email', None)
        if roll_no:
            try:
                user = User.objects.get(new_rollno=roll_no)
                data = {'roll_no': roll_no,
                        'mobile_no': user.mobile_no,
                        'rfidvalue': user.rfidvalue,
                        'parent_phone': user.parent_phone,
                        'department': user.department,
                        'batch': user.batch,
                        'entry_type': user.entry_type,
                        'stay': user.stay,
                        'email': user.email,
                        'first_name': user.first_name
                        }
                return Response(data)
            except Exception as e:
                return Response({'error': 'Roll Number Does not Exist'})
        if email:
            try:
                user = User.objects.get(email=email)
                data = {'roll_no': user.new_rollno,
                        'mobile_no': user.mobile_no,
                        'rfidvalue': user.rfidvalue,
                        'parent_phone': user.parent_phone,
                        'department': user.department,
                        'batch': user.batch,
                        'entry_type': user.entry_type,
                        'stay': user.stay,
                        'email': user.email,
                        'first_name': user.first_name
                        }
                return Response(data)
            except Exception as e:
                return Response({'error': 'Email Does not Exist'}, status=400)
        else:
            # datas = []
            # for user in User.objects.all():
            # data = {'roll_no':user.new_rollno,
            #              'mobile_no':user.mobile_no,
            #              'rfidvalue':user.rfidvalue,
            #              'parent_phone':user.parent_phone,
            #              'department':user.department,
            #              'batch':user.batch,
            #              'entry_type':user.entry_type,
            #              'stay':user.stay,
            #              'email':user.email,
            #              'first_name':user.first_name
            #              }
            # datas.append(data)
            # return Response(datas)
            return Response({'error': 'Roll Number and Email Parameter Missing'})


# def GetStudentData(request, rfid=None):
#     print("Hello")
#     try:
#         user = User.objects.get(rfidvalue=rfid)
#         data = {'roll_number': user.roll_no,
#                 'student_name': user.first_name,
#                 'department': user.department,
#                 'email': user.email,
#                 }
#         return Response(data)
#     except Exception as e:
#         return Response({'error': 'User does not exist'})


@api_view(['GET'])
@authentication_classes([BasicAuthentication])
@permission_classes([IsAuthenticated])
def get_view(request, format=None):
    content = {

        'user': str(request.user),  # `django.contrib.auth.User` instance.
        'auth': str(request.auth),  # None
    }
    return Response(content)


def manageRecord(request, stud_id=None):
    if stud_id is not None:
        instance = get_object_or_404(User, id=stud_id)
        created_user = instance.created_user
    else:
        instance = None
        created_user = None

    if request.POST:
        userRecordForm = UserRecordForm(request.POST, instance=instance)
        if userRecordForm.is_valid():

            record = userRecordForm.save()

            if instance is None:
                record.created_user = request.user
                record.edited_user = request.user
                record.save()

                messages.add_message(request, messages.SUCCESS, 'User created Successfully!!!')
            else:
                record.created_user = created_user
                record.edited_user = request.user
                record.edited_at = datetime.datetime.now()
                record.save()
                messages.add_message(request, messages.SUCCESS, 'User updated successfully!!!')
            return redirect('dashboard')
    else:
        userRecordForm = UserRecordForm(instance=instance)

    return render(request, 'student_data/manage_user_record.html', {'form': userRecordForm})


@user_passes_test(check_dashboard_access, login_url='landing')
def dashboardView(request):
    from student_data.import_data import import_data
    user_records = User.objects.filter(is_student=True, deprecated=False)

    if request.POST:
        csv_form = CSVImportForm(request.POST, request.FILES)
        print(csv_form.errors)
        if csv_form.is_valid():
            csv_data = csv_form.cleaned_data['csv_file']
            print(csv_data)
            import_data(csv_data, request.user)
            messages.add_message(request, messages.SUCCESS, 'User created Successfully!!!')
    return render(request, 'student_data/dashboard.html', {'user_records': user_records, 'csv_form': CSVImportForm})


@user_passes_test(check_admin, login_url='landing')
def give_permission(request):
    if request.method == "POST":
        user_form = UserForm(data=request.POST)
        perm_form = PermissionForm(data=request.POST)
        if user_form.is_valid() and perm_form.is_valid():
            mail = user_form.cleaned_data['email']
            try:
                user = User.objects.filter(email=mail)[0]
            except:
                messages.error(request, "No such user")
                return render(request, 'student_data/allot_permissions.html',
                              {'user_form': user_form, 'perm_form': perm_form})
            perm = perm_form.cleaned_data['perm']
            user.perms.add(perm)
            user.save()
            messages.add_message(request, messages.SUCCESS, "Permission added")
            return redirect('dashboard')

    user_form = UserForm()
    perm_form = PermissionForm()
    return render(request, 'student_data/allot_permissions.html', {'user_form': user_form, 'perm_form': perm_form})


def hideRecord(request, stud_id):
    instance = get_object_or_404(User, id=stud_id)
    instance.deprecated = True
    instance.save()
    messages.add_message(request, messages.SUCCESS, 'User deleted successfully!!!')
    return redirect('dashboard')


# class ImportDataAdminView(FormView):
#     form_class = CSVImportForm
#
#     def form_valid(self, form):
#         csv_data = form.cleaned_data['csv_file']
#
#         import_data(csv_data)
#         return super().form_valid(form)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['title'] = 'Upload CSV File'
#         return context
#
#
# def bulk_export(request):
#     form_class = CSVImportForm
#
#     if request.POST:
#         csv_data = request.POST.get('csv_file',None)
#
#         # Error handling left out for simplicity.
#         import_data(csv_data)
#         return
#
#


def import_users():
    from student_data.models import User
    with open("student_list_09_april_2022.csv", mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        print(csv_reader)
        count = 0
        for record_row in csv_reader:
            password = record_row['password']
            if password == "NULL":
                password = None
            is_superuser = record_row['is_superuser']
            if is_superuser == "NULL" or is_superuser == "FALSE":
                is_superuser = False
            else:
                is_superuser = False
            username = record_row['username']
            if username == "NULL":
                username = None
            first_name = record_row['first_name']
            if first_name == "NULL":
                first_name = None
            last_name = record_row['last_name']
            email = record_row['email']
            if email == "NULL":
                email = username + "@kct.ac.in"
            is_staff = record_row['is_staff']
            if is_staff == "NULL" or is_staff == "FALSE":
                is_staff = False
            is_active = record_row['is_active']
            if is_active == "NULL" or is_active == "TRUE":
                is_active = True
            else:
                is_active = True
            date_joined = record_row['date_joined']
            if date_joined == "NULL":
                date_joined == None
            else:
                date_joined = datetime.datetime.now()
            new_rollno = record_row['new_rollno']
            if new_rollno == "NULL":
                new_rollno = None
            mobile_no = record_row['mobile_no']
            if mobile_no == "NULL" or mobile_no == '':
                mobile_no = None
            rfidvalue = record_row['rfidvalue']
            if rfidvalue == "NULL":
                rfidvalue = None
            user_image = record_row['user_image']
            parent_phone = record_row['parent_phone']
            if parent_phone == "NULL" or parent_phone == '':
                parent_phone = None
            department = record_row['department']
            if department == "NULL":
                department = None
            batch = record_row['batch']
            if batch == "NULL":
                batch = None
            entry_type = record_row['entry_type']
            if entry_type == "NULL":
                entry_type = None
            deprecated = record_row['deprecated']
            if deprecated == "TRUE":
                deprecated = True
            elif deprecated == "FALSE":
                deprecated = False
            try:
                User.objects.create(password=password, is_superuser=is_superuser, is_staff=is_staff,
                                    is_active=is_active,
                                    username=username, first_name=first_name, last_name=last_name, email=email,
                                    created_at=date_joined, mobile_no=mobile_no, parent_phone=parent_phone,
                                    department=department, batch=batch, entry_type=entry_type, new_rollno=new_rollno,
                                    rfidvalue=rfidvalue, deprecated=deprecated)
                count += 1
                print(count)
            except Exception as e:
                print(e)
