import datetime

from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import Permission
from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _


# Create your models here.

class User(AbstractUser):
    old_rollno = models.CharField(max_length=20)
    new_rollno = models.CharField(max_length=20)
    mobile_no = models.BigIntegerField(null=True)
    rfidvalue = models.CharField(max_length=20)
    user_image = models.ImageField(upload_to='user_images/')
    parent_phone = models.BigIntegerField(null=True)
    department = models.CharField(max_length=255, null=True)
    batch = models.CharField(max_length=255, null=True)
    entry_type = models.CharField(max_length=255, null=True)
    address=models.CharField(max_length=200,blank=True,null=True)
    valid=models.CharField(max_length=100,blank=True,null=True)
    stay = models.CharField(max_length=255, null=True)
    email = models.EmailField(_('email address'), blank=True)
    first_name = models.CharField(_('first name'), max_length=255, blank=True, null=True)
    created_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                     related_name="associated_user_created", null=True,
                                     blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    edited_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                    related_name="associated_user_edited", null=True,
                                    blank=True)
    edited_at = models.DateTimeField(auto_now_add=True)
    is_student = models.BooleanField(default=False)
    deprecated = models.BooleanField(default=False)
    perms=models.ManyToManyField('UserGroup',related_name="associated_permissions",default=None, null=True, blank=True)

    class Meta:
        db_table = "Users"


class UserGroup(models.Model):
    name = models.CharField(max_length=20)
    permissions = models.ManyToManyField(Permission)

    def __str__(self):
        return self.name
