import csv
import os
from io import StringIO
from student_data.models import User
from django.conf import settings


def import_data(file_name, created_user):
    print("File Name is " + str(file_name.name))
    try:
        path = os.getcwd()
        with open(os.path.join(settings.MEDIA_ROOT, file_name.name), "wb+") as f:
            for chunk in file_name.chunks():
                f.write(chunk)
        import pandas as pd

        read_file = pd.read_excel(settings.MEDIA_ROOT + "/" + file_name.name, engine="openpyxl")

        # Write the dataframe object
        # into csv file
        read_file.to_csv(settings.MEDIA_ROOT + "/" + "convert.csv",
                         index=None,
                         header=True)

        # read csv file and convert
        # into a dataframe object
        # reader = csv.DictReader("convert.csv")
        # print(reader)
        # reader = csv.DictReader(StringIO(file_name.read().decode('utf-8-sig')))
        with open(settings.MEDIA_ROOT + "/" + "convert.csv", mode='r') as file:
            reader = csv.reader(file)
            # reader=csv.DictReader(StringIO(file.read().decode('utf-8-sig')))
            print(reader)
            dataSet = list()
            for row in reader:
                if any(x.strip() for x in row):
                    dataSet.append(row)
            # print(dataSet)
            for arr in range(1, len(dataSet)):
                row = dataSet[arr]
                try:
                    rollno, email, name, department, batch, mobile, parent_mobile, address, entry_type, stay, valid = \
                        row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10]
                    user, _ = User.objects.get_or_create(username=rollno)
                    if parent_mobile != '':
                        user.parent_phone = int(parent_mobile.split(".")[0])
                    user.department = department
                    user.batch = batch
                    if mobile != '':
                        user.mobile_no = int(mobile.split(".")[0])
                    user.first_name = name
                    user.old_rollno = rollno
                    user.new_rollno = rollno
                    user.email = email
                    user.is_student = True
                    user.entry_type = entry_type
                    user.stay = stay
                    user.address = address
                    user.valid = valid
                    user.created_user = created_user
                    user.edited_user = created_user
                    user.save()
                    print(user)
                except Exception as e:

                    print(str(e) + ' is the exception for ' + rollno)
        # os.remove(settings.MEDIA_ROOT+"/"+"convert.csv")
        # os.remove(settings.MEDIA_ROOT+"/"+file_name.name)
    except Exception as ex:
        print(str(ex) + ' is the exception')
