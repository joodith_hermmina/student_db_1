from django.urls import path
from django.contrib import admin

from .views import *
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('dashboard/', dashboardView, name='dashboard'),
    path('create_user_record', manageRecord, name="create_record"),
    path('update_user_record/<slug:stud_id>/', manageRecord, name="update_record"),
    path('delete_record/<slug:stud_id>/', hideRecord, name="hide_record"),
    # path('placement_attendance/', get_data, name='get_data'),
    path('give_permission/', give_permission, name='give_permission'),
    path('get_data/', get_data, name='get_data'),
    path('get_data_rfid/<slug:rfid>/<slug:machine_id>/', GetStudentData.as_view(), name='get_data_rfid'),
    path('submit_data/<slug:pk>/', store_rfid, name='submit_data'),
    path('get_data_students', GetData.as_view(), name='get_students_data'),

]

