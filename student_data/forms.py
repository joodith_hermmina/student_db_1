from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm
from student_data.models import User, UserGroup


class UserRecordForm(ModelForm):
    class Meta:
        model = User
        exclude = ("user_groups",)

    def __init__(self, *args, **kwargs):
        super(UserRecordForm, self).__init__(*args, **kwargs)
        not_required = ['username', 'password', 'rfidvalue', 'date_joined', 'old_rollno', 'rfidvalue', 'user_image']
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})
            if field in not_required:
                self.fields[field].required = False

    def save(self, commit=True):
        record = super(UserRecordForm, self).save(commit=False)
        record.is_student = True
        record.old_rollno = self.cleaned_data['new_rollno']
        record.rfidvalue = self.cleaned_data['new_rollno']
        record.username = self.cleaned_data['new_rollno']
        if commit:
            record.save()
        return record


class CSVImportForm(forms.Form):
    csv_file = forms.FileField()

    def __init__(self, *args, **kwargs):
        super(CSVImportForm, self).__init__(*args, **kwargs)
        self.fields['csv_file'].widget.attrs.update({'class': 'form-control'})


class UserForm(forms.ModelForm):
    class Meta:
        model=User
        fields=("email",)

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.update({'class': 'form-control'})



class PermissionForm(forms.Form):
    perm = forms.ModelChoiceField(queryset=UserGroup.objects.all())

    def __init__(self, *args, **kwargs):
        super(PermissionForm, self).__init__(*args, **kwargs)
        self.fields['perm'].widget.attrs.update({'class': 'form-control'})


class LoginForm(forms.Form):
    email = forms.CharField(widget=forms.EmailInput())
