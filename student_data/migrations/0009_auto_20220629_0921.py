# Generated by Django 3.1.4 on 2022-06-29 16:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('student_data', '0008_auto_20220509_1131'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='perms',
            field=models.ManyToManyField(blank=True, default=None, null=True, related_name='associated_permissions', to='student_data.UserGroup'),
        ),

    ]
