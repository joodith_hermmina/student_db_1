# Generated by Django 3.1.4 on 2022-06-29 23:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('student_data', '0010_auto_20220629_1547'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='address',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
