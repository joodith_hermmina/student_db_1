from django.contrib import admin
from student_data.models import User,UserGroup
from django.contrib.auth.models import Permission
admin.site.site_url = "/students/get_data"
# admin.site.register(User)
admin.site.register(UserGroup)
admin.site.register(Permission)

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    #list display
    list_display = ['username','department']
    #list Filter
    list_filter = ('username',)
    # search list
    search_fields = ['username']

