import csv


from student_data.models import User


def import_data():
    with open('student_data_cleaned.csv', 'r') as f:
        reader = csv.DictReader(f)

        for row in reader:
            try:
                rollno, email, name, department, batch, mobile, parent_mobile = row['rollno'], row['email'], row[
                    'name'], \
                                                                                row['department'], row['batch'], row[
                                                                                    'mobile'], row['parent_mobile']
                user, _ = User.objects.get_or_create(new_rollno=rollno)
                if parent_mobile != '':
                    user.parent_phone = int(parent_mobile)
                user.department = department
                user.batch = batch
                if mobile != '':
                    user.mobile_no = int(mobile)
                user.first_name = name
                user.rfidvalue = rollno
                user.username = rollno
                user.email = email
                user.save()

            except Exception as e:
                print(str(e) + ' is the exception for ' + row['rollno'])
